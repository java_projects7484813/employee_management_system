package java_sql;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import javax.swing.JRadioButton;
public class Read_Find extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tid;
	private JTextField tname;
	private JTextField tage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Read_Find frame = new Read_Find();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
Connection con;
PreparedStatement pt;
ResultSet rs;
	
	public void Connect() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/praveen";
			String userName="root";
			String password="system";
			con=DriverManager.getConnection(url,userName,password);
			}
		catch(Exception e){
			
		}
	}

	/**
	 * Create the frame.
	 */
	public Read_Find() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 573, 337);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Employee Details");
		lblNewLabel.setBounds(209, 26, 129, 30);
		contentPane.add(lblNewLabel);
		
		JLabel Id = new JLabel("Id");
		Id.setBounds(87, 88, 32, 14);
		contentPane.add(Id);
		
		tid = new JTextField();
		tid.setBounds(221, 85, 86, 20);
		contentPane.add(tid);
		tid.setColumns(10);
		
		JLabel Name = new JLabel("Name");
		Name.setBounds(87, 129, 46, 14);
		contentPane.add(Name);
		
		JLabel Age = new JLabel("Age");
		Age.setBounds(87, 174, 32, 14);
		contentPane.add(Age);
		
		tname = new JTextField();
		tname.setBounds(221, 126, 86, 20);
		contentPane.add(tname);
		tname.setColumns(10);
		
		tage = new JTextField();
		tage.setBounds(221, 171, 86, 20);
		contentPane.add(tage);
		tage.setColumns(10);
		
		JButton find = new JButton("Find");
		find.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					pt=con.prepareStatement("select * from edetail where id=?");
					pt.setInt(1, Integer.parseInt(tid.getText()));
					rs=pt.executeQuery();
					
					if(rs.next()==true) {
						String pa=rs.getString("name");
						String pri=rs.getString("age");
						tname.setText(pa);
						tage.setText(pri);
						
					}
					else {
						tname.setText("not found");
						tage.setText("not found");
					}
					}
				catch(Exception ae){
					
				}
			}
		});
		find.setBounds(394, 84, 89, 23);
		contentPane.add(find);
		
		JButton Add = new JButton("Add");
		Add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id=Integer.parseInt(tid.getText());
				String name=tname.getText();
				int age=Integer.parseInt(tage.getText());
				
				try {
					pt=con.prepareStatement("insert into edetail(id,name,age)values(?,?,?)");
					pt.setInt(1, id);
					pt.setString(2, name);
					pt.setInt(3, age);
					pt.executeUpdate();
					JOptionPane.showMessageDialog(null, "Successfully Added");
				} 
				catch (SQLException e1) {
					
					e1.printStackTrace();
				}
			}
		});
		Add.setBounds(394, 125, 89, 23);
		contentPane.add(Add);
		
		JButton Update = new JButton("Update");
		Update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id=Integer.parseInt(tid.getText());
				String name=tname.getText();
				int age=Integer.parseInt(tage.getText());
				
				try {
					pt=con.prepareStatement("update edetail set name=?, age=? where id=?");
					
					pt.setString(1, name);
					pt.setInt(2, age);
					pt.setInt(3, id);
					pt.executeUpdate();
					JOptionPane.showMessageDialog(null, "Successfully Updated");
				} 
				catch (SQLException e1) {
					
					e1.printStackTrace();
				}
			}
		});
		Update.setBounds(394, 165, 89, 23);
		contentPane.add(Update);
		
		JButton Delete = new JButton("Delete");
		Delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id=Integer.parseInt(tid.getText());
				try {
					pt=con.prepareStatement("delete from edetail where id=?");
					
					
					pt.setInt(1, id);
					pt.executeUpdate();
					JOptionPane.showMessageDialog(null, "Successfully Deleted");
				} 
				catch (SQLException e1) {
					
					e1.printStackTrace();
				}
				
			}
		});
		Delete.setBounds(126, 264, 89, 23);
		contentPane.add(Delete);
		
		JButton Clear = new JButton("Clear");
		Clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tid.setText("");
				tname.setText("");
				tage.setText("");
				tid.requestFocus();
				
			}
		});
		Clear.setBounds(286, 264, 89, 23);
		contentPane.add(Clear);
		
		JLabel Gender = new JLabel("Gender");
		Gender.setBounds(87, 221, 46, 14);
		contentPane.add(Gender);
		
		JRadioButton Male = new JRadioButton("Male");
		Male.setBounds(160, 217, 66, 23);
		contentPane.add(Male);
		
		JRadioButton Female = new JRadioButton("Female");
		Female.setBounds(241, 212, 66, 32);
		contentPane.add(Female);
		Connect();
	}
}
